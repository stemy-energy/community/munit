/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Include MUnit function definitions
#include "munit/munit.h"

int main(void) {
    // Test programs must notify the framework by calling tests start
    munit_tests_start();
    
    // Perform any interesting checks...
    munit_assert("test_a_very_simple_statement", 1 == 1);
    munit_assert("test_something_not_quite_right", 6 == 1);
    
    // When finished call tests finish and that is it!
    munit_tests_finish();
    
    return 0;
}
