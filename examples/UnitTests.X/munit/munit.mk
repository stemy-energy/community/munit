# Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# MUnit testing makefile hooks.
.PHONY: munit_post_build
.build-post: munit_post_build

# Detect if we are building on windows...
PLATFORM_WINDOWS =
ifeq ($(shell uname), windows32)
    PLATFORM_WINDOWS = True
endif
ifeq ($(OS), Windows_NT)
    PLATFORM_WINDOWS = True
endif

# Dynamically find the path to this makefile
thisMakefilePath := $(realpath $(lastword $(MAKEFILE_LIST)))
thisMakefileDir  := $(dir $(thisMakefilePath))

# Random hack to have access to variables...
ifeq (,$(findstring nbproject/Makefile-${CONF}.mk,$(MAKEFILE_LIST)))
include nbproject/Makefile-local-${CONF}.mk
endif
ifeq (,$(findstring nbproject/Makefile-${CONF}.mk,$(MAKEFILE_LIST)))
IGNORE_LOCAL:=TRUE
include nbproject/Makefile-${CONF}.mk
IGNORE_LOCAL:=FALSE
endif
export
unexport IGNORE_LOCAL

# Path commands
space := $(subst ,, )
scape_spaces = $(subst $(space),\$(space),$1)
ifeq (${PLATFORM_WINDOWS}, True)
cleanpath = $(subst /,\,$(subst ",,$(strip $1)))
classpathify = $(subst " ",;,$1)
else
cleanpath = $(subst ",,$(strip $1))
classpathify = $(subst " ",:,$1)
endif

# Get MPLAB installation directory from Java path.
mplabPlatformPath := $(call cleanpath,$(MP_JAVA_PATH))../../../../mplab_platform/
ifeq ($(wildcard $(call scape_spaces,$(call cleanpath,${mplabPlatformPath}.))),)
mplabPlatformPath := $(call cleanpath,$(MP_JAVA_PATH))../../../../../mplab_platform/
endif

# List of jar dependencies for MUnit
mplabbJarPaths := mdbcore/modules/ext/ mdbcore/modules/ mplablibs/modules/ext/ mplablibs/modules/ platform/lib/ platform/core/ ide/modules/
absMplabJarPaths := $(patsubst %,"$(mplabPlatformPath)%*",$(mplabbJarPaths))
allJarPaths := $(absMplabJarPaths) "$(thisMakefileDir)MUnit.jar"
javaClasspath := $(call classpathify,$(allJarPaths))

# Main java unit
javaMainUnit := munit.MUnit

# Post-build code to run MUnit.jar
munit_post_build: .build-impl
ifneq ($(IMAGE_TYPE),debug)
	${MP_JAVA_PATH}java -classpath ${javaClasspath} ${javaMainUnit} -f "dist/${CONF}/${IMAGE_TYPE}/${PROJECTNAME}.${IMAGE_TYPE}.elf"
else
	@echo "[+] Tests not launched to allow debugging!"
endif
