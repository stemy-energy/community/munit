/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package munit;

import com.microchip.mplab.logger.MPLABLogger;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.logging.Level;
import munit.mdbhelpers.Device;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Main program class. Contains the entry function. Handles argument parsing and
 * is responsible to scan the environment to detect MPLAB X installation
 * information.
 */
public class MUnit {

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();

        Options options = new Options();
        options.addOption(Option.builder("d")
                .longOpt("device")
                .hasArg()
                .optionalArg(false)
                .desc("Device model to run the tests on (Defaults to dsPIC33FJ256MC710A)")
                .type(String.class)
                .required(false)
                .build());
        options.addOption(Option.builder("f")
                .longOpt("firmware")
                .hasArg()
                .optionalArg(false)
                .desc("Binary file containing the testing firmware (elf, hex or coff)")
                .type(String.class)
                .required(true)
                .build());
        options.addOption(Option.builder("m")
                .longOpt("mplab")
                .hasArg()
                .optionalArg(false)
                .desc("MPlab X installation folder")
                .type(String.class)
                .required(false)
                .build());
        options.addOption(Option.builder("l")
                .longOpt("loglevel")
                .hasArg()
                .optionalArg(false)
                .desc("Logging level from -1 (quiet) to 7 (debug).")
                .type(Integer.class)
                .required(false)
                .build());

        try {
            CommandLine line = parser.parse(options, args);

            Level mplabLogLevel = Level.SEVERE;
            String crownkingLogLevel = "very-quiet";
            if (line.hasOption("loglevel")) {
                Integer l = Integer.valueOf(line.getOptionValue("loglevel"));
                if (l >= -1 && l <= 7) {
                    mplabLogLevel = (l == -1) ? Level.OFF
                            : (l == 0) ? Level.SEVERE
                                    : (l == 1) ? Level.WARNING
                                            : (l == 2) ? Level.INFO
                                                    : (l == 3) ? Level.CONFIG
                                                            : (l == 4) ? Level.FINE
                                                                    : (l == 5) ? Level.FINER
                                                                            : (l == 6) ? Level.FINEST : Level.ALL;
                    crownkingLogLevel = (l == -1) ? "very-quiet"
                            : (l == 0) ? "very-quiet"
                                    : (l == 1) ? "quiet"
                                            : (l == 2) ? "quiet"
                                                    : (l == 3) ? "quiet"
                                                            : (l == 4) ? "verbose"
                                                                    : (l == 5) ? "very-verbose"
                                                                            : (l == 6) ? "very-verbose" : "very-verbose";
                } else {
                    System.out.println("[!] Unrecognized log level. Please specify a number between -1 and 7.");
                    System.out.println("[!] Defaulting to 0 loglevel.");
                }
            }
            System.setProperty("crownking.stream.verbosity", crownkingLogLevel);
            MPLABLogger.setLogLevel(mplabLogLevel);

            String mplab_dir = null;
            if (!line.hasOption("mplab")) {
                System.out.println("[+] MPlab X installation dir not provided. Trying to detect it.");
                mplab_dir = getMplabDir();
            }
            if (mplab_dir != null) {
                String packs = mplab_dir + "\\packs";
                System.out.println("[+] Packs folder: " + packs);
                System.setProperty("packslib.packsfolder", packs);
            }

            if (!line.hasOption("device")) {
                System.out.println("[w] Device not set. Using default option...");
            }
            String device = Device.fixName(line.getOptionValue("d", "dsPIC33FJ256MC710A"));
            System.out.println("[+] Device: " + device);

            String firmware = line.getOptionValue("firmware");
            File f = new File(firmware);
            if (!f.exists() || !f.isFile()) {
                System.out.println("[!] Could not locate firmware file: " + firmware);
                System.exit(1);
            }
            System.out.println("[+] Using firmware: " + firmware);

            System.out.println("[+] Setting up the test...");
            TestRunner t = new TestRunner(device, firmware);

            System.out.println("[+] Runing the test...");
            t.run();

        } catch (ParseException exp) {
            System.out.println(exp.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("MUnit", options);
            System.exit(1);
        } catch (InterruptedException ex) {
            System.out.println("[!] Interrupted! Exiting...");
        }

        System.exit(0);
    }

    private static String getMplabDir() {
        // First attempt
        File path = new File(System.getenv("ProgramFiles") + "\\Microchip\\MPLABX");
        if (path.exists() && path.isDirectory()) {
            String[] subdirs = path.list(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return new File(current, name).isDirectory();
                }
            });
            Arrays.sort(subdirs);
            String subdir = subdirs[subdirs.length - 1];
            String install_dir = path.getAbsolutePath() + "\\" + subdir;
            return install_dir;
        }
        // TODO System.out.println(env.get("ProgramFiles(x86)"));
        return null;
    }
}
