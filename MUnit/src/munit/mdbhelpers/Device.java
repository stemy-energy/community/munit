/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package munit.mdbhelpers;

/**
 * Device MDB helper functions.
 * The following class contains helper functions to interact with MDB device 
 * core functions.
 */
public class Device {
    
    /**
     * Attempt to fix common errors in device names and return the correct form.
     * @param d Desired device name.
     * @return A better device name that fits Microchip standards.
     */
    public static String fixName(String d) {
        boolean dirty = true;
        if (d.startsWith("DS")) {
            d = d.replaceFirst("DS", "ds");
        } else if (d.startsWith("ATMEGA")) {
            d = d.replaceFirst("ATMEGA", "ATmega");
        } else if (d.startsWith("ATTINY")) {
            d = d.replaceFirst("ATTINY", "ATtiny");
        } else if (d.startsWith("ATMEGA")) {
            d = d.replaceFirst("ATXMEGA", "ATxmega");
        } else {
            dirty = false;
        }
        if (dirty) {
            System.out.println("[w] Fixing device name to correct form: " + d);
        }
        return d;
    }
}
