/*
 * Copyright (C) 2021 Antonio Vázquez Blanco <antonio@stemyenergy.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package munit.mdbhelpers;

import com.microchip.mplab.comm.MPLABCommProviderInterface;
import com.microchip.mplab.mdbcore.platformtool.PlatformToolMeta;
import com.microchip.mplab.mdbcore.platformtool.PlatformToolMetaManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.openide.util.Lookup;

public class Tool {

    public static List<String> getAllSupportedTools() {
        List<String> toolNames = new ArrayList<String>();
        List<PlatformToolMeta> metas = PlatformToolMetaManager.getAllTools();
        for (PlatformToolMeta m : metas) {
            toolNames.add(m.getName());
        }
        return toolNames;
    }

    public static PlatformToolMeta getToolMetaFromName(String name) {
        List<PlatformToolMeta> metas = PlatformToolMetaManager.getAllTools();
        for (PlatformToolMeta m : metas) {
            if (m.getName().equals(name)) {
                return m;
            }
        }
        return null;
    }

    public static void getConnectedTools() {
        // TODO: Implement...
        MPLABCommProviderInterface Com = Lookup.getDefault().lookup(MPLABCommProviderInterface.class);
        Map<Integer, String> toolMap = Com.GetCurrentToolList();
        String ToolId = toolMap.get(0);
    }

    public static String getConnectedToolIdFromName(String name) {
        if (name.equals("Simulator")) {
            return "Simulator";
        } else {
            // TODO: Implement...
            return null;
        }
    }
}
